# Website Design
## General info
This project's goal is exercising front-end programming.
One for log in and other one for main page.
I have used **HTML5** and **CSS3**.
It's not connected to any back-end code and It's just a prototype of the site.

## Technologies
* CSS3
* HTML5